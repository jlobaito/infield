module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),

    	concat: {
    		dist: {
    			src: [
            'resources/js/*.js', // All JS in the libs folder
            'resources/js/libs/*.js',
            'resources/js/vendor/*.js'
            ],
            dest: 'js/build/production.js',
        }
    },
    uglify: {
    	build: {
    		src: 'js/build/production.js',
    		dest: 'js/build/production.min.js'
    	}
    },
    imagemin: {
    	dynamic: {
    		files: [{
    			expand: true,
    			cwd: 'resources/img',
    			src: ['**/*.{png,jpg,gif}'],
    			dest: 'images/build/'
    		}]
    	}
    },
    sass: {
    	dist: {
    		options: {
    			style: 'compressed'
    		},
    		files: {
    			'resources/css/base.css': 'resources/sass/base.scss'
    		}
    	} 
    },
    watch: {
    	options: {
    		livereload: true,
    	},
    	scripts: {
    		files: [
    		'resources/js/*.js', 
    		'resources/js/libs/*.js',
    		'resources/js/vendor/*.js'
    		],
    		tasks: ['concat', 'uglify'],
    		options: {
    			spawn: false,
    		},
    	} ,
    	css: {
    		files: ['resources/sass/*.scss'],
    		tasks: ['sass'],
    		options: {
    			spawn: false,
    		}
    	}
    }

});

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'imagemin', 'sass', 'watch']);

};